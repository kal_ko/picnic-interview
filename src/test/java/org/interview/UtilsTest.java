package org.interview;

import com.google.api.client.repackaged.com.google.common.annotations.VisibleForTesting;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Artem on 06.01.2017.
 */
public class UtilsTest {
    @Test
    public void testDate() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
        formatter.setLenient(true);

        Date date = formatter.parse("Fri Jan 06 02:45:25 +0000 2017");
        System.out.println(date);
    }
}
