package org.interview;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.common.collect.Multimap;
import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.UncheckedTimeoutException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.interview.domain.Tweet;
import org.interview.domain.TweetAuthor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Artem on 13.01.2017.
 */
public class TweetLoader {
    private final Log log = LogFactory.getLog(TweetLoader.class);

    private final HttpRequestFactory requestFactory;
    private final String streamingURL;
    private final ExecutorService executor;

    public TweetLoader(HttpRequestFactory requestFactory, String streamingURL) {
        this.requestFactory = requestFactory;
        this.streamingURL = streamingURL;

        executor = Executors.newCachedThreadPool();
    }

    public InputStream getTweetsStream(String searchString) throws IOException {
        GenericUrl url = new GenericUrl(streamingURL);
        url.set("track", searchString);

        if (log.isDebugEnabled()) {
            log.debug("Requesting tweets from URL: " + url.toString());
        }

        HttpRequest request = requestFactory.buildPostRequest(url, null);
        HttpResponse response = request.execute();

        log.info("Response status is " + response.getStatusCode());

        return response.getContent();
    }

    public Collection<Tweet> findTweets(String searchString, int maxTweets, int timeout) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getTweetsStream(searchString)))) {
            List<Tweet> tweets = new ArrayList<>();

            AtomicInteger loadedTweets = new AtomicInteger(0);
            new SimpleTimeLimiter(executor).callWithTimeout(() -> {
                while (loadedTweets.get() < maxTweets && !Thread.interrupted()) {
                    String tweetString = reader.readLine();
                    if (log.isDebugEnabled()) {
                        log.debug("Received tweet: " + tweetString);
                    }
                    Tweet tweet = JSONUtils.parseTweet(tweetString);
                    if (tweet != null) {
                        tweets.add(tweet);
                        loadedTweets.incrementAndGet();
                    }
                }
                return null;
            }, timeout, TimeUnit.SECONDS, true);

            return tweets;
        }
        catch (UncheckedTimeoutException e) {}
        catch (Exception e){
            log.error("Error getting tweets for '" + searchString + "'", e);
        }
        return Collections.EMPTY_LIST;
    }
}
