package org.interview.domain;

/**
 * Created by Artem on 05.01.2017.
 */
public class TweetAuthor {
    private long id;
    private long createdAt;
    private String name;
    private String displayName;

    public TweetAuthor(long id, long createdAt, String name, String displayName) {
        this.id = id;
        this.createdAt = createdAt;
        this.name = name;
        this.displayName = displayName;
    }

    public long getId() {
        return id;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TweetAuthor that = (TweetAuthor) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "TweetAuthor{" +
                "name='" + name + '\'' +
                '}';
    }
}
