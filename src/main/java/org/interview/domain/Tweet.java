package org.interview.domain;

/**
 * Created by Artem on 05.01.2017.
 */
public class Tweet {
    private long id;
    private TweetAuthor author;
    private String text;
    private long createdAt;

    public Tweet(long id, TweetAuthor author, String text, long createdAt) {
        this.id = id;
        this.author = author;
        this.text = text;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public TweetAuthor getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tweet tweet = (Tweet) o;

        return id == tweet.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "'" + text + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
