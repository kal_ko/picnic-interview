package org.interview;

import com.google.gson.ExclusionStrategy;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.interview.domain.Tweet;
import org.interview.domain.TweetAuthor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Artem on 06.01.2017.
 */
public class JSONUtils {
    private static final Log log = LogFactory.getLog(JSONUtils.class);

    private static JsonParser jsonParser = new JsonParser();
    private static SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);

    public static TweetAuthor parseAuthor(String json) {
        try {
            JsonObject tweetJsonObject = jsonParser.parse(json).getAsJsonObject();
            JsonObject authorJsonObject = tweetJsonObject.get("user").getAsJsonObject();

            return new TweetAuthor(authorJsonObject.get("id").getAsLong(),
                    formatter.parse(authorJsonObject.get("created_at").getAsString()).getTime(),
                    authorJsonObject.get("name").getAsString(),
                    authorJsonObject.get("screen_name").getAsString());
        } catch (Exception e) {
            log.error("Error parsing JSON for tweet", e);
            return null;
        }
    }

    public static Tweet parseTweet(String json) {
        try {
            JsonObject tweetJsonObject = jsonParser.parse(json).getAsJsonObject();
            JsonObject authorJsonObject = tweetJsonObject.get("user").getAsJsonObject();
            TweetAuthor author = new TweetAuthor(authorJsonObject.get("id").getAsLong(),
                    formatter.parse(authorJsonObject.get("created_at").getAsString()).getTime(),
                    authorJsonObject.get("name").getAsString(),
                    authorJsonObject.get("screen_name").getAsString());

            return new Tweet(tweetJsonObject.get("id").getAsLong(),
                    author,
                    tweetJsonObject.get("text").getAsString(),
                    formatter.parse(tweetJsonObject.get("created_at").getAsString()).getTime());
        } catch (Exception e) {
            log.error("Error parsing JSON for tweet", e);
            return null;
        }
    }
}
