package org.interview;

import com.google.api.client.http.*;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.interview.domain.Tweet;
import org.interview.domain.TweetAuthor;
import org.interview.oauth.twitter.TwitterAuthenticationException;
import org.interview.oauth.twitter.TwitterAuthenticator;

import java.io.*;
import java.util.*;

/**
 * Created by Artem on 05.01.2017.
 */
public class TweetLoaderApp {
    private static Log log = LogFactory.getLog(TweetLoaderApp.class);

    private static final String PROP_FILE_NAME = "config.properties";

    public static void main(String... args) {
        Properties prop = new Properties();

        InputStream inputStream = TweetLoaderApp.class.getClassLoader().getResourceAsStream(PROP_FILE_NAME);

        try {
            prop.load(inputStream);
        } catch (NullPointerException | IOException e) {
            log.error("Can't load properties from " + PROP_FILE_NAME, e);
            return;
        }

        TwitterAuthenticator authenticator = new TwitterAuthenticator(System.out, prop.getProperty("consumerKey"), prop.getProperty("consumerSecret"));

        try {
            HttpRequestFactory factory = authenticator.getAuthorizedHttpRequestFactory();
            TweetLoader tweetLoader = new TweetLoader(factory, prop.getProperty("streamingURL"));

            Collection<Tweet> tweets = tweetLoader.findTweets(
                    prop.getProperty("searchString"),
                    Integer.parseInt(prop.getProperty("maxTweets", "100")),
                    Integer.parseInt(prop.getProperty("maxTimeout", "30")));

            log.info("Tweets loaded: " + tweets.size());

            printTweets(System.out, tweets);
        } catch (TwitterAuthenticationException e) {
            log.error("Twitter auth error", e);
        } catch (NumberFormatException e) {
            log.error("Error in properties, maxTweets and maxTimeout should be integer", e);
        }
    }

    private static void printTweets(PrintStream out, Collection<Tweet> tweets) {
        Multimap<TweetAuthor, Tweet> tweetsSortedMap = TreeMultimap.create(Comparator.comparing(TweetAuthor::getCreatedAt), Comparator.comparing(Tweet::getCreatedAt));

        tweets.forEach(tweet -> tweetsSortedMap.put(tweet.getAuthor(), tweet));

        for (TweetAuthor author : tweetsSortedMap.keySet()) {
            out.println(author.toString() + author.getCreatedAt());
            for (Tweet tweet : tweetsSortedMap.get(author)) {
                out.println("\t" + tweet.toString());
            }
            out.println();
        }
    }
}
